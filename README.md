# Ansible into docker container

This project creates multiple `ansible` version containers.

## Tags

When this var is set (you can overwrite it in `.gitlab-ci.yml` or in
[CI/CD vars menu](https://gitlab.com/devopsworks/ansible/ansible-in-docker/-/settings/ci_cd)) :

```yaml
ANSIBLE_MIN_VERSION: "2.3"
```

Docker container with `ansible` version from `2.3` to `latest` (2.9 today) are created automatically :

- 2.3 (EOL)
- 2.4 (EOL)
- 2.5 (EOL)
- 2.6 (EOL)
- 2.7 (Security fixes)
- 2.8 (security and critical bug fixes)
- 2.9 (security and general bug fixes)
- 2.10
- 2.12
- latest (2.12 today)

[Ansible Release status](https://docs.ansible.com/ansible/latest/reference_appendices/release_and_maintenance.html#release-status)

[Ansible Roadmap](https://docs.ansible.com/ansible/devel/roadmap/)

Tools like `molecule` or `ansible-lint` are installed too.

## Use

This is useful for CI/CD with `ansible playbooks`. Just call image
[`registry.gitlab.com/devopsworks/ansible/ansible-in-docker:X.X`](https://gitlab.com/devopsworks/ansible/ansible-in-docker/container_registry/).

Info :

```Dockerfile
CMD [ "ansible", "--help" ]
ENTRYPOINT [ "/usr/local/bin/entrypoint.sh" ]
```

## Upgrade / Build

To build a container with the latest version of `ansible` available, just run a
pipeline (for example if `ansible` 2.10 is out).

If you don't want to build older versions, just set the `ANSIBLE_MIN_VERSION` to the version
you want the starting build. For example `ANSIBLE_MIN_VERSION: 2.6` build :

- 2.6
- 2.7
- 2.8
- 2.9
