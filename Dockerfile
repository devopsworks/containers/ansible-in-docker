FROM debian:bullseye-slim

ARG ANSIBLE_CURRENT_VERSION_TO_INSTALL=2.9

ENV DEBIAN_FRONTEND=noninteractive

ADD entrypoint.sh /usr/local/bin/entrypoint.sh

RUN apt-get update -qq \
  && apt-get install -qqy python3-dev python3-pip python3-setuptools python3-openssl python3-crcmod sshpass git libffi-dev libssl-dev > /dev/null \
  && rm -rf /var/lib/apt/lists/* \
  && echo "==========> Installing ansible ${ANSIBLE_CURRENT_VERSION_TO_INSTALL}" \
  && pip3 install -q --no-cache-dir ansible-inventory-grapher ansible-lint ansible~=${ANSIBLE_CURRENT_VERSION_TO_INSTALL}.0 apache-libcloud appdirs boto boto3 docker-py httplib2 ipaddr Jinja2 molecule netaddr packaging paramiko python-vagrant PyYAML six yamllint \
  && chmod +x /usr/local/bin/entrypoint.sh \
  && update-alternatives --install /usr/bin/pip pip /usr/bin/pip3 1 \
  && update-alternatives --install /usr/bin/python python /usr/bin/python3 1 \
  && pip -V \
  && python -V \
  && ansible --version

CMD [ "ansible", "--help" ]
ENTRYPOINT [ "/usr/local/bin/entrypoint.sh" ]
